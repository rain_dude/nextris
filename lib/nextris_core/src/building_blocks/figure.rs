use crate::building_blocks::size::Size;

use super::block::Block;
use crate::building_blocks::block::Status;
use crate::building_blocks::rows::{Rows, Row};

#[derive(Debug, Eq, PartialEq)]
pub struct Figure {
    rows: Rows,
}

impl Figure {
    pub fn occupied(size: Size) -> Figure {
        Figure {
            rows: Rows::occupied(size),
        }
    }

    pub fn one() -> Figure {
        Figure::occupied(Size::one())
    }

    pub fn wrap(rows: Rows) -> Figure {
        Figure { rows }
    }

    pub fn from_separate_rows(rows: Vec<Row>) -> Figure {
        let rows = Rows::wrap(rows);

        Figure::wrap(rows)
    }

    pub fn from_blocks(blocks: Vec<Vec<Block>>) -> Figure {
        let rows = blocks.into_iter()
            .map(|row| Row::wrap(row))
            .collect();

        Figure::from_separate_rows(rows)
    }

    pub fn blocks(&self) -> &Rows {
        &self.rows
    }

    pub fn width(&self) -> usize {
        self.rows.content()
            .iter()
            .map(|row| row.content().capacity())
            .max()
            .unwrap_or(0)
    }

    pub fn height(&self) -> usize {
        self.rows.content().capacity()
    }
}


#[cfg(test)]
mod tests {
    use std::fmt;
    use std::fmt::{Display, Formatter};

    use crate::building_blocks::block::Status;

    use super::*;

    #[test]
    fn test_figure_shape() {
        let figure = Figure::one();
        let expected_figure = Figure::occupied(Size::one());

        assert_eq!(figure, expected_figure)
    }
}
