use crate::building_blocks::size::Size;
use crate::building_blocks::block::{Block, Status};

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Rows {
    content: Vec<Row>,
}

impl Rows {
    pub fn occupied(size: Size) -> Rows {
        Rows {
            content: vec![Row::occupied_of_width(size.width()); size.height()],
        }
    }

    pub fn of_size(size: &Size) -> Rows {
        Rows {
            content: vec![
                Row::occupied_of_width(size.width());
                size.height()]
        }
    }

    pub fn wrap(content: Vec<Row>) -> Rows {
        Rows { content }
    }

    pub fn content(&self) -> &Vec<Row> {
        &self.content
    }

    pub fn first_row(&self) -> &Row {
        self.content
            .first()
            .unwrap()
    }

    pub fn count(&self) -> usize {
        self.content
            .capacity()
    }

    pub fn at(&self, i: usize) -> Option<&Row> {
        self.content
            .get(i)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Row {
    content: Vec<Block>,
}

impl Row {
    fn occupied_of_width(width: usize) -> Row {
        Row {
            content: vec![
                Block::occupied();
                width],
        }
    }

    pub fn from_statuses(statuses: Vec<Status>) -> Row {
        let blocks = statuses
            .into_iter()
            .map(|status| Block::of(status))
            .collect();

        Row::wrap(blocks)
    }

    pub fn wrap(content: Vec<Block>) -> Row {
        Row { content }
    }

    pub fn content(&self) -> &Vec<Block> {
        &self.content
    }
}
