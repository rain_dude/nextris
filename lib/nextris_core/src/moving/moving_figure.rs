use crate::building_blocks::board::Board;
use crate::building_blocks::figure::Figure;
use crate::building_blocks::block::Block;
use crate::building_blocks::block::Status::Occupied;

pub enum Direction {
    Up,
    Right,
    Down,
    Left,
}

trait MoveFigure {
    fn move_figure(self, direction: &Direction) -> Board;
}

impl MoveFigure for Board {
    fn move_figure(self, direction: &Direction) -> Board {
        let moved = move_in_direction(self.figure(), direction);
        match moved {
            Some(figure) => self.with_figure(figure),
            None => self
        }
    }
}

fn move_in_direction(figure: &Figure, direction: &Direction) -> Option<Figure> {
    match direction {
        Direction::Up => move_up(figure),
        Direction::Right => move_right(figure),
        Direction::Down => move_down(figure),
        Direction::Left => move_left(figure),
    }
}

fn move_up(figure: &Figure) -> Option<Figure> {
    let rows = figure.blocks();
    let first_row_content = rows.first_row()
        .content();
    if is_not_empty(first_row_content) {
        return None;
    }

    let (head, tail) = rows.content()
        .split_at(1);
    let mut moved = Vec::new();
    moved.extend_from_slice(tail);
    moved.extend_from_slice(head);

    Some(
        Figure::from_separate_rows(moved)
    )
}

fn move_right(figure: &Figure) -> Option<Figure> {
    None
}

fn move_down(figure: &Figure) -> Option<Figure> {
    None
}

fn move_left(figure: &Figure) -> Option<Figure> {
    None
}

fn is_not_empty(blocks: &Vec<Block>) -> bool {
    blocks.iter()
        .map(|block| block.status())
        .any(|status| *status == Occupied)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_move_up_returns_moved_when_movable() {
        let figure = Figure::from_blocks([
            [Block::empty()].to_vec(),
            [Block::occupied()].to_vec(),
            [Block::occupied()].to_vec(),
        ].to_vec());

        let moved = move_up(&figure);

        let expected = Figure::from_blocks([
            [Block::occupied()].to_vec(),
            [Block::occupied()].to_vec(),
            [Block::empty()].to_vec(),
        ].to_vec());
        let expected = Some(expected);

        assert_eq!(moved, expected)
    }

    #[test]
    fn test_move_up_returns_not_moved_when_unmovable() {
        let figure = Figure::from_blocks([
            [Block::occupied()].to_vec(),
            [Block::occupied()].to_vec(),
            [Block::empty()].to_vec(),
        ].to_vec());

        let moved = move_up(&figure);

        let expected = None;

        assert_eq!(moved, expected)
    }

    // TODO kolejne metody
}
