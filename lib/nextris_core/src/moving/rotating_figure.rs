use crate::building_blocks::figure::Figure;
use crate::moving::rotating_figure::Rotation::{Once, Twice, Thrice, Original};
use std::collections::HashMap;


struct RotatingFigure<'fig> {
    variants: &'fig HashMap<Rotation, &'fig Figure>,
    rotation: Rotation,
    figure: &'fig Figure,
}

impl<'fig> RotatingFigure<'fig> {
    fn new(variants: &'fig HashMap<Rotation, &'fig Figure>) -> RotatingFigure {
        let rotation = Rotation::Original;
        let figure = variants.get(&rotation).unwrap();

        RotatingFigure {
            variants,
            rotation,
            figure,
        }
    }

    fn rotate(self) -> RotatingFigure<'fig> {
        let next = self.rotation.next();
        let next_figure = self.variants.get(&next).unwrap();

        RotatingFigure {
            variants: self.variants,
            rotation: next,
            figure: &next_figure,
        }
    }
}


#[derive(Debug, PartialEq, Eq, Hash)]
enum Rotation {
    Original,
    Once,
    Twice,
    Thrice,
}

impl Rotation {
    fn next(&self) -> Rotation {
        match self {
            Original => Once,
            Once => Twice,
            Twice => Thrice,
            Thrice => Original,
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use crate::building_blocks::size::Size;

    #[test]
    fn test_figure_1x2_rotates_to_2x1() {
        let variant_1 = Figure::occupied(Size::of(1, 2));
        let variant_2 = Figure::occupied(Size::of(2, 1));

        let mut variants: HashMap<Rotation, &Figure> = HashMap::new();
        variants.insert(Original, &variant_1);
        variants.insert(Once, &variant_2);
        variants.insert(Twice, &variant_1);
        variants.insert(Thrice, &variant_2);

        let rotating_figure = RotatingFigure::new(&variants);

        let original = rotating_figure.figure;
        let rotated = rotating_figure.rotate().figure;

        assert_eq!(rotated, &variant_2)
    }

    #[test]
    fn test_original_next_4_times_gives_original() {
        let original = Rotation::Original;
        let full_circle = Rotation::Original
            .next() // 1
            .next() // 2
            .next() // 3
            .next() // 4
            ;

        assert_eq!(original, full_circle)
    }
}
